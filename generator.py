import cv2
import os
import sys
import subprocess
import hashlib
import string
import numpy as np 
import random

from libgen.trdg.data_generator import FakeTextDataGenerator
from libgen.trdg import background_generator
from libgen.trdg.generators import (
    GeneratorFromDict,
    GeneratorFromRandom,
    GeneratorFromStrings,
    GeneratorFromWikipedia,
)
from libgen.trdg.string_generator import (
    create_strings_from_file,
    create_strings_from_dict,
    create_strings_from_wikipedia,
    create_strings_randomly,
)

class OCRDataGenerator(object):
    def __init__(self, min_seq_len, max_seq_len, corpus_path='demo-full.txt', \
                font_type='vi_basic', num_epochs=10, discard_last_samples=False):
        """
            min_seq_len: Min sentence's length uses in corpus
            max_seq_len: Max sentence's length uses in corpus
            corpus_path: Path to corpus
            font_type: 'vi_basic' to use basic font otherwise uses 'vi'
            batch_size: Batch size 
            num_epochs: Number of epochs
            discard_last_samples: Whether discard last batch of data or not?
        """
        self.min_seq_len = min_seq_len
        self.max_seq_len = max_seq_len
        self.corpus_path = corpus_path
        self.font_type = font_type
        self.num_epochs = num_epochs
        self.discard_last_samples = discard_last_samples
        self.list_sentences = self.load_corpus()

    def load_corpus(self):
        list_sentences = []
        with open(self.corpus_path, 'r') as f:
            for line in f:
                line = line.replace('\n', '')
                if len(line) >= self.min_seq_len and len(line) <= self.max_seq_len:
                    list_sentences.append(line)
        return list_sentences

    def load_generator(self):
        generator = GeneratorFromStrings(
            self.list_sentences,
            count=len(self.list_sentences),
            blur=4,
            random_blur=True,
            size=70,
            skewing_angle=3,
            random_skew=True,
            distorsion_orientation=1,
            language=self.font_type
        )
        return generator


    def augment_corpus(self, corpus):
        augmented_corpus = []
        for sentence in corpus:
            if random.random() < .6:
                # Random cut sentence with probability of 30%
                if (random.random() < .4) and (len(sentence) > 3):
                    _sentence = sentence
                    n = len(sentence)
                    max_n = int(max(n / 2, 1))
                    cut_len = random.randint(1, max_n)
                    start_idx = random.randint(0, n - cut_len)
                    sentence = sentence[start_idx:start_idx + cut_len]
                    sentence = sentence.strip()
                    if len(sentence) < 1:
                        sentence = _sentence

                # Random upper all character with p = 10%
                if random.random() < .1:
                    sentence = sentence.upper()
                else:
                    # Random lower all character with p = 10%
                    if random.random() < .1:
                        sentence = sentence.lower()

                # Random lower/upper with p = 20%
                if random.random() < .2:
                    _sentence = ''
                    for c in sentence:
                        if random.random() < .5:
                            _sentence += c.upper()
                        else:
                            _sentence += c.lower()
                    sentence = _sentence
                augmented_corpus.append(sentence)
            else:
                augmented_corpus.append(str(sentence))
        return augmented_corpus


    def gen(self, batch_size=16, discard_last_samples=False):
        for i in range(self.num_epochs):
            np.random.shuffle(self.list_sentences)
            
            self.list_sentences = self.augment_corpus(self.list_sentences)

            self.generator = self.load_generator()
            batch_images = []
            batch_labels = []

            for img, lbl in self.generator:
                batch_images.append(img)
                batch_labels.append(lbl)

                if len(batch_images) >= batch_size:
                    yield batch_images, batch_labels
                    batch_images = []
                    batch_labels = []
            if not self.discard_last_samples and len(batch_images) > 0:
                yield batch_images, batch_labels
