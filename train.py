import numpy as np 
import generator
import cv2

data_gen = generator.OCRDataGenerator(min_seq_len=5, max_seq_len=15, num_epochs=3)

batch_size = 16
steps_per_epoch = int(len(data_gen.list_sentences) / 16) + len(data_gen.list_sentences) % 16
step = 0
for batch_images, batch_labels in data_gen.gen():
    step += 1
    print('Step: ', step, '. Epoch: ', int((step - 1)/steps_per_epoch) + 1)
    for img, lbl in zip(batch_images, batch_labels):
        print(lbl)
        cv2.imshow('image', np.array(img))
        cv2.waitKey(0)